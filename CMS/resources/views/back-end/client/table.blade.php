@foreach($clients as $client)
    <tr>
        <td>{{ $client->id }}</td>
        <td>{{ $client->title }}</td>
        <td><img src="{{ $client->image }}" alt="Image" style="max-width: 50px"></td>
        <td>{{ $client->link }}</td>
        <td>
            <button type="button"
                    class="btn btn-info edit"
                    data-toggle="modal"
                    client-id="{{ $client->id }}"
                    client-edit-url="{{ route('client.edit', $client) }}"
                    data-target="#client-model">
                Edit
            </button>
            <a href="" class="btn btn-danger">Delete</a>
        </td>
    </tr>
@endforeach
