@extends('back-end.layouts.app')

@section('pageHeader', 'Client')

@section('content')
    <div class="col-12">
        <div class="card p-3">
            <!-- Button trigger modal -->
            <button id="btn-client-create" client-create-url="{{ route('client.create') }}" type="button" class="btn btn-success" data-toggle="modal" data-target="#client-model">
                Create
            </button>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Image</th>
                    <th>Link</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
{{--                    @include('back-end.client.table')--}}
                </tbody>
            </table>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade"
         id="client-model" tabindex="-1"
         role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="client-form">

                </div>
            </div>
        </div>
    </div>
@endsection


@section('footer_js')
    <script>
        $(document).on('click', '.edit', function () {
            // var clientId = $(this).attr('client-id');
            var url = $(this).attr('client-edit-url');
            $.ajax({
                type: 'GET',
                url: url,
                success: function (res) {
                    $('#client-form').html(res);
                }
            })
        });

        $('#btn-client-create').click(function () {
            var url = $(this).attr('client-create-url');
            $.ajax({
                type: 'GET',
                url: url,
                success: function (res) {
                    $('#client-form').html(res);
                }
            })
        });

        $(document).on('submit', '#update-client', function (e) {
            e.preventDefault();
            var method = $(this).attr('method');
            var url = $(this).attr('action');
            var data = $(this).serialize();

            $.ajax({
                type: method,
                url:url,
                data: data,
                success: function (res) {
                    $('#client-model').modal('hide');
                    getTable();
                }
            })
        });
        getTable();
        function getTable() {
            $.ajax({
                type: 'GET',
                url: '{{ route("client.getTable") }}',
                success: function (res) {
                    $('table tbody').html(res);
                }
            })
        }
    </script>
@endsection
