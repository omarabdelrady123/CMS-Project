<form action="{{ route('client.update', $client) }}" id="update-client" method="post" enctype="multipart/form-data">
    <div class="modal-body">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control" value="{{ $client->title }}" name="title" id="title">
        </div>
        <div class="form-group">
            <label for="link">Link</label>
            <input type="text" class="form-control" value="{{ $client->link }}" name="link" id="link">
        </div>
        <div class="form-group">
            <label for="image">Image</label>
            <input type="file" class="form-control" name="image" id="image">
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
    </div>
</form>
